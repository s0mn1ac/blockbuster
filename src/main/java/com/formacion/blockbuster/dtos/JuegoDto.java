package com.formacion.blockbuster.dtos;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.formacion.blockbuster.enums.EnumCategoria;
import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;

import lombok.Data;

@Data
public class JuegoDto {
	
	private long id;
	
	@NotBlank(message = DataErrorMessages.TITULO_NOT_BLANK)
	private String titulo;
	
	@NotNull(message = DataErrorMessages.FECHALANZAMIENTO_NOT_NULL)
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/Madrid")
	private LocalDate fechaLanzamiento;
	
	@NotNull(message = DataErrorMessages.PEGI_NOT_NULL)
	@Positive
	private Integer pegi;
	
	@NotNull(message = DataErrorMessages.CATEGORIA_NOT_NULL)
	private EnumCategoria categoria;
	
	@NotNull(message = DataErrorMessages.DESARROLLADORAS_NOT_NULL)
	private List<DesarrolladoraDto> desarrolladoras;
	
}