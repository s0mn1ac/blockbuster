package com.formacion.blockbuster.dtos;

import java.util.List;

import lombok.Data;

@Data
public class ResponseErrorDto {
	
	private String exception;
	private String message;
	private String path;
	private List<String> valids;
	
	public ResponseErrorDto(Exception exception, String path) {
		this.exception = exception.getClass().getSimpleName();
		this.message = exception.getMessage();
		this.path = path;
	}
	
	public ResponseErrorDto(Exception exception, List<String> messages, String path) {
		this.exception = exception.getClass().getSimpleName();
		this.valids = messages;
		this.path = path;
	}

}
