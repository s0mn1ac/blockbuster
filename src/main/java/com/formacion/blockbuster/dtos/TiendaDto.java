package com.formacion.blockbuster.dtos;

import javax.validation.constraints.NotBlank;

import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;

import lombok.Data;

@Data
public class TiendaDto {
	
	private long id;
	
	@NotBlank(message = DataErrorMessages.NOMBRE_NOT_BLANK)
	private String nombre;
	
	@NotBlank(message = DataErrorMessages.DIRECCION_NOT_BLANK)
	private String direccion;

}