package com.formacion.blockbuster.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.format.annotation.NumberFormat;

import com.formacion.blockbuster.enums.EnumEstado;
import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;

import lombok.Data;

@Data
public class PedidoDto {
	
	private long id;
	
	@NotNull(message = DataErrorMessages.CODREFERENCIA_NOT_NULL)
	@NumberFormat
	@Positive
	private Integer codReferencia;
	
	@NotNull(message = DataErrorMessages.ESTADO_NOT_NULL)
	private EnumEstado estado;
	
	@NotNull(message = DataErrorMessages.JUEGO_NOT_NULL)
	private JuegoDto juego;
	
	@NotNull(message = DataErrorMessages.CLIENTE_NOT_NULL)
	private ClienteDto cliente;
	
	@NotNull(message = DataErrorMessages.TIENDA_NOT_NULL)
	private TiendaDto tienda;

}
