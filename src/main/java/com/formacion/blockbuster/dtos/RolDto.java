package com.formacion.blockbuster.dtos;

import javax.validation.constraints.NotNull;

import com.formacion.blockbuster.enums.EnumRol;
import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;

import lombok.Data;

@Data
public class RolDto {
	
	private long id;
	
	@NotNull(message = DataErrorMessages.ROLES_NOT_NULL)
	private EnumRol nombre;

}
