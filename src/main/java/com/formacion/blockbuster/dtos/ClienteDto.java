package com.formacion.blockbuster.dtos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;
import com.formacion.blockbuster.validations.Documento;

import lombok.Data;

@Data
public class ClienteDto {
	
	private long id;
	
	@Documento
	private String documento;
	
	@NotBlank(message = DataErrorMessages.NOMBRE_NOT_BLANK)
	private String nombre;
	
	@NotNull(message = DataErrorMessages.FECHANAC_NOT_NULL)
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/Madrid")
	private LocalDate fechaNac;
	
	@NotBlank(message = DataErrorMessages.CORREO_NOT_BLANK)
	@Email(message = DataErrorMessages.CORREO_NOT_VALID)
	private String correo;
	
	@NotBlank(message = DataErrorMessages.USER_NOT_BLANK)
	private String user;
	
	@NotBlank(message = DataErrorMessages.PASS_NOT_BLANK)
	private String pass;
	
	@NotNull(message = DataErrorMessages.ROLES_NOT_NULL)
	private List<RolDto> roles = new ArrayList<RolDto>();
	
	private String img;
		
}