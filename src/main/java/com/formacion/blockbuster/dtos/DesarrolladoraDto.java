package com.formacion.blockbuster.dtos;

import javax.validation.constraints.NotBlank;

import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;

import lombok.Data;

@Data
public class DesarrolladoraDto {
	
	private long id;
	
	@NotBlank(message = DataErrorMessages.CIF_NOT_BLANK)
	private String cif;
	
	@NotBlank(message = DataErrorMessages.NOMBRE_NOT_BLANK)
	private String nombre;
	
	private String img;

}