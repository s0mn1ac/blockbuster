package com.formacion.blockbuster.converters;

import com.formacion.blockbuster.dtos.ClienteDto;
import com.formacion.blockbuster.dtos.DesarrolladoraDto;
import com.formacion.blockbuster.dtos.JuegoDto;
import com.formacion.blockbuster.dtos.PedidoDto;
import com.formacion.blockbuster.dtos.RolDto;
import com.formacion.blockbuster.dtos.TiendaDto;
import com.formacion.blockbuster.entities.Cliente;
import com.formacion.blockbuster.entities.Desarrolladora;
import com.formacion.blockbuster.entities.Juego;
import com.formacion.blockbuster.entities.Pedido;
import com.formacion.blockbuster.entities.Rol;
import com.formacion.blockbuster.entities.Tienda;

public interface DtoEntityConverter {

	public PedidoDto pedidoEntityToDto(Pedido p);
	public Pedido pedidoDtoToEntity(PedidoDto pedido);
	public ClienteDto clienteEntityToDto(Cliente c);
	public Cliente clienteDtoToEntity(ClienteDto cliente);
	public JuegoDto juegoEntityToDto(Juego j);
	public Juego juegoDtoToEntity(JuegoDto juego);
	public DesarrolladoraDto desarrolladoraEntityToDto(Desarrolladora d);
	public Desarrolladora desarrolladoraDtoToEntity(DesarrolladoraDto desarrolladora);
	public TiendaDto tiendaEntityToDto(Tienda t);
	public Tienda tiendaDtoToEntity(TiendaDto tienda);
	public RolDto rolEntityToDto(Rol r);
	public Rol rolDtoToEntity(RolDto rol);

}
