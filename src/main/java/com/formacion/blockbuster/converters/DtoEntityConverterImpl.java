package com.formacion.blockbuster.converters;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.formacion.blockbuster.dtos.ClienteDto;
import com.formacion.blockbuster.dtos.DesarrolladoraDto;
import com.formacion.blockbuster.dtos.JuegoDto;
import com.formacion.blockbuster.dtos.PedidoDto;
import com.formacion.blockbuster.dtos.RolDto;
import com.formacion.blockbuster.dtos.TiendaDto;
import com.formacion.blockbuster.entities.Cliente;
import com.formacion.blockbuster.entities.Desarrolladora;
import com.formacion.blockbuster.entities.Juego;
import com.formacion.blockbuster.entities.Pedido;
import com.formacion.blockbuster.entities.Rol;
import com.formacion.blockbuster.entities.Tienda;
import com.formacion.blockbuster.enums.EnumCategoria;
import com.formacion.blockbuster.enums.EnumEstado;
import com.formacion.blockbuster.enums.EnumRol;
import com.formacion.blockbuster.exceptions.ClienteNotFoundException;
import com.formacion.blockbuster.exceptions.JuegoNotFoundException;
import com.formacion.blockbuster.exceptions.TiendaNotFoundException;
import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;
import com.formacion.blockbuster.repositories.ClienteRepository;
import com.formacion.blockbuster.repositories.JuegoRepository;
import com.formacion.blockbuster.repositories.TiendaRepository;

@Service
public class DtoEntityConverterImpl implements DtoEntityConverter {
	
	@Autowired
	private BCryptPasswordEncoder enc;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private JuegoRepository juegoRepository;
	
	@Autowired
	private TiendaRepository tiendaRepository;
	
	
	// ----- PEDIDOS --------------------------------------------------------------------------------------------------------------
	
	@Override
	public PedidoDto pedidoEntityToDto(Pedido p) {
		
		PedidoDto pedido = new PedidoDto();
		
		pedido.setId(p.getIdPedido());
		pedido.setCodReferencia(p.getCodReferencia());
		pedido.setEstado(EnumEstado.valueOf(p.getEstado()));
		pedido.setCliente(clienteEntityToDto(clienteRepository.findByDocumento(p.getCliente().getDocumento()).orElseThrow(() -> new ClienteNotFoundException(DataErrorMessages.CLIENTE_NOT_FOUND))));
		pedido.setJuego(juegoEntityToDto(juegoRepository.findByTitulo(p.getJuego().getTitulo()).orElseThrow(() -> new JuegoNotFoundException(DataErrorMessages.JUEGO_NOT_FOUND))));
		pedido.setTienda(tiendaEntityToDto(tiendaRepository.findByNombre(p.getTienda().getNombre()).orElseThrow(() -> new TiendaNotFoundException(DataErrorMessages.TIENDA_NOT_FOUND))));
		
		return pedido;
		
	}
	
	@Override
	public Pedido pedidoDtoToEntity(PedidoDto pedido) {
		
		Pedido p = new Pedido();
		
		p.setEstado(pedido.getEstado().toString());
		p.setCodReferencia(pedido.getCodReferencia());
		
		return p;
		
	}
	
	
	
	// ----- CLIENTES -------------------------------------------------------------------------------------------------------------
	
	@Override
	public ClienteDto clienteEntityToDto(Cliente c) {

		ClienteDto cliente = new ClienteDto();
		
		cliente.setId(c.getIdCliente());
		cliente.setNombre(c.getNombre());
		cliente.setDocumento(c.getDocumento());
		cliente.setFechaNac(c.getFechaNac());
		cliente.setCorreo(c.getCorreo());
		cliente.setUser(c.getUser());
		cliente.setRoles(listaRoles(c));
		cliente.setImg(c.getImg());
		
		return cliente;
		
	}
	
	@Override
	public Cliente clienteDtoToEntity(ClienteDto cliente) {
		
		Cliente c = new Cliente();
		
		c.setNombre(cliente.getNombre());
		c.setDocumento(cliente.getDocumento());
		c.setFechaNac(cliente.getFechaNac());
		c.setCorreo(cliente.getCorreo());
		c.setUser(cliente.getUser());
		c.setPass(enc.encode(cliente.getPass()));
		
		return c;
		
	}
	
	private ArrayList<RolDto> listaRoles(Cliente c) {
		
		ArrayList<RolDto> lista = new ArrayList<>();

		for(Rol rol : c.getRoles()) {
			RolDto r = new RolDto();
			r.setId(rol.getIdRol());
			r.setNombre(EnumRol.valueOf(rol.getNombre()));
			lista.add(r);
		}
		return lista;

	}
	
	
	
	// ----- JUEGOS ---------------------------------------------------------------------------------------------------------------
	
	@Override
	public JuegoDto juegoEntityToDto(Juego j) {
		
		JuegoDto juego = new JuegoDto();
		
		juego.setId(j.getIdJuego());
		juego.setTitulo(j.getTitulo());
		juego.setCategoria(EnumCategoria.valueOf(j.getCategoria().toString()));
		juego.setFechaLanzamiento(j.getFechaLanzamiento());
		juego.setPegi(j.getPegi());
		juego.setDesarrolladoras(listaDesarrolladoras(j));
		return juego;
		
	}
	
	@Override
	public Juego juegoDtoToEntity(JuegoDto juego) {
		
		Juego j = new Juego();
		
		j.setTitulo(juego.getTitulo());
		j.setCategoria(juego.getCategoria().toString());
		j.setFechaLanzamiento(juego.getFechaLanzamiento());
		j.setPegi(juego.getPegi());
		
		return j;
		
	}
	
	private ArrayList<DesarrolladoraDto> listaDesarrolladoras(Juego j) {
		
		ArrayList<DesarrolladoraDto> lista = new ArrayList<>();

		for(Desarrolladora desarrolladora : j.getDesarrolladoras()) {
			DesarrolladoraDto d = new DesarrolladoraDto();
			d.setId(desarrolladora.getIdDesarrolladora());
			d.setCif(desarrolladora.getCif());
			d.setNombre(desarrolladora.getNombre());
			d.setImg(desarrolladora.getImg());
			lista.add(d);
		}
		return lista;

	}
	
	
	
	// ----- DESARROLLADORAS ------------------------------------------------------------------------------------------------------
	
	@Override
	public DesarrolladoraDto desarrolladoraEntityToDto(Desarrolladora d) {
		
		DesarrolladoraDto desarrolladora = new DesarrolladoraDto();
		
		desarrolladora.setId(d.getIdDesarrolladora());
		desarrolladora.setCif(d.getCif());
		desarrolladora.setNombre(d.getNombre());
		desarrolladora.setImg(d.getImg());
		
		return desarrolladora;
		
	}
	
	@Override
	public Desarrolladora desarrolladoraDtoToEntity(DesarrolladoraDto desarrolladora) {
		
		Desarrolladora d = new Desarrolladora();
		
		d.setCif(desarrolladora.getCif());
		d.setNombre(desarrolladora.getNombre());
		
		return d;
		
	}
	
	
	
	// ----- TIENDAS --------------------------------------------------------------------------------------------------------------
	
	@Override
	public TiendaDto tiendaEntityToDto(Tienda t) {
		
		TiendaDto tienda = new TiendaDto();
		
		tienda.setId(t.getIdTienda());
		tienda.setNombre(t.getNombre());
		tienda.setDireccion(t.getDireccion());
		
		return tienda;
		
	}
	
	@Override
	public Tienda tiendaDtoToEntity(TiendaDto tienda) {
		
		Tienda t = new Tienda();
		
		t.setNombre(tienda.getNombre());
		t.setDireccion(tienda.getDireccion());
		
		return t;
		
	}
	
	
	
	// ----- ROLES ----------------------------------------------------------------------------------------------------------------
	
	@Override
	public RolDto rolEntityToDto(Rol r) {
		
		RolDto rol = new RolDto();
		
		rol.setId(r.getIdRol());
		rol.setNombre(EnumRol.valueOf(r.getNombre()));
		
		return rol;
	}
	
	@Override
	public Rol rolDtoToEntity(RolDto rol) {
		
		Rol r = new Rol();
		
		r.setNombre(rol.getNombre().toString());
		
		return r;
	}
	
}
