package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class ClienteNotFoundException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	
	public ClienteNotFoundException(String details) {
		super(details);
	}

}
