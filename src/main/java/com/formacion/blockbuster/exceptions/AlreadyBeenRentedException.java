package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class AlreadyBeenRentedException extends BadRequestException {

	private static final long serialVersionUID = 1L;
	
	public AlreadyBeenRentedException(String details) {
		super(details);
	}

}
