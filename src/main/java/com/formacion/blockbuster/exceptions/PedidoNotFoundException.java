package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class PedidoNotFoundException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	
	public PedidoNotFoundException(String details) {
		super(details);
	}

}
