package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.NoContentException;

public class TiendaNoContentException extends NoContentException {
	
	private static final long serialVersionUID = 1L;
	
	public TiendaNoContentException(String details) {
		super(details);
	}

}
