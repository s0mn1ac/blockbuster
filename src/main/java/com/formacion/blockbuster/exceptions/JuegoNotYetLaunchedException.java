package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class JuegoNotYetLaunchedException extends BadRequestException {

	private static final long serialVersionUID = 1L;
	
	public JuegoNotYetLaunchedException(String details) {
		super(details);
	}

}
