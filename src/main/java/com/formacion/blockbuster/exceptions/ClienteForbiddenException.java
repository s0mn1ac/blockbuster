package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.ForbiddenException;

public class ClienteForbiddenException extends ForbiddenException {
	
	private static final long serialVersionUID = 1L;
	
	public ClienteForbiddenException(String details) {
		super(details);
	}

}