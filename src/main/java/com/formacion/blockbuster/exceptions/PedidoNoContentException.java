package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.NoContentException;

public class PedidoNoContentException extends NoContentException {
	
	private static final long serialVersionUID = 1L;
	
	public PedidoNoContentException(String details) {
		super(details);
	}

}