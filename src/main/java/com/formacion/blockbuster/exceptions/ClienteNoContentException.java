package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.NoContentException;

public class ClienteNoContentException extends NoContentException {
	
	private static final long serialVersionUID = 1L;
	
	public ClienteNoContentException(String details) {
		super(details);
	}

}
