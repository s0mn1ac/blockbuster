package com.formacion.blockbuster.exceptions.generic;

public class ForbiddenException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ForbiddenException(String details) {
		super(details);
	}
	
}
