package com.formacion.blockbuster.exceptions.generic;

public class NotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public NotFoundException(String details) {
		super(details);
	}
	
}
