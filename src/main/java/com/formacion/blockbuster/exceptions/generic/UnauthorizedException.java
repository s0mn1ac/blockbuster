package com.formacion.blockbuster.exceptions.generic;

public class UnauthorizedException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public UnauthorizedException(String details) {
		super(details);
	}

}
