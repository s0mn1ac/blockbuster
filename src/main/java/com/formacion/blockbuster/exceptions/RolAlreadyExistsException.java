package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class RolAlreadyExistsException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	
	public RolAlreadyExistsException(String details) {
		super(details);
	}

}
