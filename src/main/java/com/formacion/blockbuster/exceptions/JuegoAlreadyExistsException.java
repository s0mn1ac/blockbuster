package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class JuegoAlreadyExistsException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	
	public JuegoAlreadyExistsException(String details) {
		super(details);
	}

}