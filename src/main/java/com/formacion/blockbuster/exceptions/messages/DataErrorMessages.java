package com.formacion.blockbuster.exceptions.messages;

public class DataErrorMessages {
	
	// ----- VALIDACIONES ---------------------------------------------------------------------------------------------------------
	
	public static final String TITULO_NOT_BLANK = "El campo TÍTULO no puede estar vacío.";
	public static final String NOMBRE_NOT_BLANK = "El campo NOMBRE no puede estar vacío.";
	public static final String DIRECCION_NOT_BLANK = "El campo DIRECCIÓN no puede estar vacío.";
	public static final String CIF_NOT_BLANK = "El campo CIF no puede estar vacío.";
	public static final String CATEGORIA_NOT_NULL = "El campo CATEGORÍA no puede estar vacío.";
	public static final String PEGI_NOT_NULL = "El campo PEGI no puede estar vacío.";
	public static final String CORREO_NOT_BLANK = "El campo CORREO no puede estar vacío.";
	public static final String DOCUMENTO_NOT_BLANK = "El campo DOCUMENTO no puede estar vacío.";
	public static final String FECHANAC_NOT_NULL = "El campo FECHANAC no puede estar vacío.";
	public static final String FECHALANZAMIENTO_NOT_NULL = "El campo FECHALANZAMIENTO no puede estar vacío.";
	public static final String CODREFERENCIA_NOT_NULL = "El campo CODREFERENCIA no puede estar vacío.";
	public static final String ESTADO_NOT_NULL = "El campo ESTADO no puede estar vacío.";
	public static final String JUEGO_NOT_NULL = "El campo JUEGO no puede estar vacío.";
	public static final String CLIENTE_NOT_NULL = "El campo CLIENTE no puede estar vacío.";
	public static final String TIENDA_NOT_NULL = "El campo TIENDA no puede estar vacío.";
	public static final String DESARROLLADORAS_NOT_NULL = "El campo DESARROLLADORAS no puede estar vacío.";
	public static final String CORREO_NOT_VALID = "El CORREO introducido no es válido.";
	public static final String DOCUMENTO_NOT_VALID = "El número de DOCUMENTO introducido no es válido.";
	public static final String DOCUMENTO_ALREADY_BEEN_USED = "El número de DOCUMENTO introducido ya está siendo utilizado.";
	public static final String USER_NOT_BLANK = "El campo USUARIO no puede estar vacío.";
	public static final String PASS_NOT_BLANK = "El campo CONTRASEÑA no puede estar vacío.";
	public static final String ROLES_NOT_NULL = "El campo ROLES no puede estar vacío.";
	
	
	
	// ----- ERRORES --------------------------------------------------------------------------------------------------------------

	public static final String NOT_OLD_ENOUGH = "No tienes la EDAD suficiente para alquilar este juego.";
	public static final String ALREADY_BEEN_RENTED = "No puedes alquilar más de un JUEGO a la vez.";
	public static final String NOT_BUYING_NOR_RENTING = "Solo puedes ALQUILAR o COMPRAR un JUEGO.";
	public static final String PEDIDO_NOT_FOUND = "El PEDIDO especificado no está registrado en la base de datos.";
	public static final String CLIENTE_NOT_FOUND = "El CLIENTE especificado no está registrado en la base de datos.";
	public static final String ROL_NOT_FOUND = "El ROL especificado no está registrado en la base de datos.";
	public static final String JUEGO_NOT_FOUND = "El JUEGO especificado no está registrado en la base de datos.";
	public static final String TIENDA_NOT_FOUND = "La TIENDA especificada no está registrado en la base de datos.";
	public static final String DESARROLLADORA_NOT_FOUND = "La DESARROLLADORA especificada no está registrado en la base de datos.";
	public static final String NOT_FOUND = "Los datos facilitados no son correctos.";
	public static final String USER_ALREADY_BEEN_USED = "El USUARIO introducido ya está siendo utilizado.";
	public static final String NOMBRE_ALREADY_BEEN_USED = "El NOMBRE especificado ya está siendo utilizado";
	public static final String TITULO_ALREADY_BEEN_USED = "El TÍTULO especificado ya está siendo utilizado";
	public static final String CIF_ALREADY_BEEN_USED = "El CIF especificado ya está siendo utilizado";
	public static final String JUEGO_NOT_READY = "El JUEGO indicado aún no está disponible";
	public static final String PEDIDO_NO_CONTENT = "No existen PEDIDOS asociados a este cliente.";
	public static final String CLIENTE_NO_CONTENT = "Sin contenido.";
	public static final String JUEGO_NO_CONTENT = "Sin contenido.";
	public static final String TIENDA_NO_CONTENT = "Sin contenido.";
	public static final String DESARROLLADORA_NO_CONTENT = "Sin contenido.";
	public static final String CLIENTE_FORBIDDEN = "Acceso restringido al área de administración de CLIENTES.";
	public static final String JUEGO_FORBIDDEN = "Acceso restringido al área de administración de JUEGOS.";
	public static final String PEDIDO_FORBIDDEN = "Acceso restringido al área de administración de PEDIDOS.";
	public static final String DESARROLLADORA_FORBIDDEN = "Acceso restringido al área de administración de DESARROLLADORAS.";
	public static final String TIENDA_FORBIDDEN = "Acceso restringido al área de administración de TIENDAS.";
	public static final String FORBIDDEN = "Acceso restringido. Solo está permitido el acceso a administradores.";
	public static final String LESS_THAN_ZERO = "El valor no puede ser menor que 0. Se sustituirá automáticamente el valor por 0.";
	
	
	
	// ----- TRANSACCIONES --------------------------------------------------------------------------------------------------------

	public static final String ENDED_TRANSACTION = "Transacción finalizada";
	public static final String CLIENTE_CREATED = "El CLIENTE ha sido dado de alta correctamente.";
	public static final String CLIENTE_MODIFIED = "El CLIENTE ha sido modificado correctamente.";
	public static final String CLIENTE_DELETED = "El CLIENTE ha sido dado de baja correctamente.";
	public static final String JUEGO_CREATED = "El JUEGO ha sido dado de alta correctamente.";
	public static final String JUEGO_MODIFIED = "El JUEGO ha sido modificado correctamente.";
	public static final String JUEGO_DELETED = "El JUEGO ha sido dado de baja correctamente.";
	public static final String DESARROLLADORA_CREATED = "La DESARROLLADORA ha sido dada de alta correctamente.";
	public static final String DESARROLLADORA_MODIFIED = "La DESARROLLADORA ha sido modificada correctamente.";
	public static final String DESARROLLADORA_DELETED = "La DESARROLLADORA ha sido dada de baja correctamente.";
	public static final String TIENDA_CREATED = "La TIENDA ha sido dada de alta correctamente.";
	public static final String TIENDA_MODIFIED = "La TIENDA ha sido modificada correctamente.";
	public static final String TIENDA_DELETED = "La TIENDA ha sido dada de baja correctamente.";
	public static final String PEDIDO_CREATED = "El PEDIDO ha sido dado de alta correctamente.";
	public static final String PEDIDO_DELETED = "El PEDIDO ha sido dado de baja correctamente.";
	public static final String ROL_CREATED = "El ROL ha sido dado de alta correctamente.";
	public static final String ROL_MODIFIED = "El ROL ha sido modificado correctamente.";
	public static final String ROL_DELETED = "El ROL ha sido dado de baja correctamente.";
	
}
