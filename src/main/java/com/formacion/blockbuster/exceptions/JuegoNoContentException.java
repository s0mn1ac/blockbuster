package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.NoContentException;

public class JuegoNoContentException extends NoContentException {
	
	private static final long serialVersionUID = 1L;
	
	public JuegoNoContentException(String details) {
		super(details);
	}

}
