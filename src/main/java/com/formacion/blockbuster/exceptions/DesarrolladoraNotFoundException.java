package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class DesarrolladoraNotFoundException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	
	public DesarrolladoraNotFoundException(String details) {
		super(details);
	}

}
