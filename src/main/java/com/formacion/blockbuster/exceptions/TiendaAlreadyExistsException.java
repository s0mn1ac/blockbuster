package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class TiendaAlreadyExistsException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	
	public TiendaAlreadyExistsException(String details) {
		super(details);
	}

}
