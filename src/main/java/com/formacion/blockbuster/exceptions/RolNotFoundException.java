package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class RolNotFoundException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	
	public RolNotFoundException(String details) {
		super(details);
	}

}
