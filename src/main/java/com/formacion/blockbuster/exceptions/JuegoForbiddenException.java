package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.ForbiddenException;

public class JuegoForbiddenException extends ForbiddenException {
	
	private static final long serialVersionUID = 1L;
	
	public JuegoForbiddenException(String details) {
		super(details);
	}

}
