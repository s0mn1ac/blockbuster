package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class JuegoNotFoundException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	
	public JuegoNotFoundException(String details) {
		super(details);
	}

}
