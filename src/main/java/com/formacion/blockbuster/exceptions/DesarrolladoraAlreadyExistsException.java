package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class DesarrolladoraAlreadyExistsException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	
	public DesarrolladoraAlreadyExistsException(String details) {
		super(details);
	}

}
