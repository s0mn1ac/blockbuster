package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.ForbiddenException;

public class DesarrolladoraForbiddenException extends ForbiddenException {
	
	private static final long serialVersionUID = 1L;
	
	public DesarrolladoraForbiddenException(String details) {
		super(details);
	}

}