package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.NoContentException;

public class DesarrolladoraNoContentException extends NoContentException {
	
	private static final long serialVersionUID = 1L;
	
	public DesarrolladoraNoContentException(String details) {
		super(details);
	}

}
