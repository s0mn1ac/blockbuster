package com.formacion.blockbuster.exceptions.handler;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.formacion.blockbuster.dtos.ResponseErrorDto;
import com.formacion.blockbuster.exceptions.generic.BadRequestException;
import com.formacion.blockbuster.exceptions.generic.ForbiddenException;
import com.formacion.blockbuster.exceptions.generic.NoContentException;
import com.formacion.blockbuster.exceptions.generic.NotFoundException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class BlockBusterHandlerException extends ResponseEntityExceptionHandler {
		
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({NotFoundException.class})
	@ResponseBody
	public ResponseErrorDto notFoundException(HttpServletRequest request, Exception exception) {
		return new ResponseErrorDto(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ExceptionHandler({NoContentException.class})
	@ResponseBody
	public ResponseErrorDto noContentException(HttpServletRequest request, Exception exception) {
		return new ResponseErrorDto(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({
		org.springframework.dao.DuplicateKeyException.class,
		javax.validation.ConstraintViolationException.class,
		BadRequestException.class})
	@ResponseBody
	public ResponseErrorDto badRequestException(HttpServletRequest request, Exception exception) {
		return new ResponseErrorDto(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler({ForbiddenException.class})
	@ResponseBody
	public ResponseErrorDto forbiddenException(HttpServletRequest request, Exception exception) {
		return new ResponseErrorDto(exception, request.getRequestURI());
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> errorMessages = ex.getBindingResult().getFieldErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
		return new ResponseEntity<Object>(new ResponseErrorDto(ex, errorMessages, ((ServletWebRequest)request).getRequest().getRequestURI()), status);
	}
	
}