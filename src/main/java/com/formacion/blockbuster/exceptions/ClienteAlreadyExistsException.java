package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class ClienteAlreadyExistsException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	
	public ClienteAlreadyExistsException(String details) {
		super(details);
	}

}
