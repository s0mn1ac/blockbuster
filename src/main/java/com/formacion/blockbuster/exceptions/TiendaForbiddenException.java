package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.ForbiddenException;

public class TiendaForbiddenException extends ForbiddenException {
	
	private static final long serialVersionUID = 1L;
	
	public TiendaForbiddenException(String details) {
		super(details);
	}

}