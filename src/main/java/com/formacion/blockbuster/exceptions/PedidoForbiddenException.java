package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.ForbiddenException;

public class PedidoForbiddenException extends ForbiddenException {
	
	private static final long serialVersionUID = 1L;
	
	public PedidoForbiddenException(String details) {
		super(details);
	}

}
