package com.formacion.blockbuster.exceptions;

import com.formacion.blockbuster.exceptions.generic.BadRequestException;

public class EdadNotValidException extends BadRequestException {

	private static final long serialVersionUID = 1L;
	
	public EdadNotValidException(String details) {
		super(details);
	}

}
