package com.formacion.blockbuster.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "JUEGOS")
@Data
public class Juego {
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idJuego;
	
	@Column(name = "TITULO")
	private String titulo;
	
	@Column(name = "FECHA_LANZAMIENTO")
	private LocalDate fechaLanzamiento;
	
	@Column(name = "PEGI")
	private Integer pegi;
	
	@Column(name = "CATEGORIA")
	private String categoria;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "juego")
	private List<Pedido> pedidos = new ArrayList<Pedido>();
	
	@ManyToMany(mappedBy = "juegos")
	private List<Desarrolladora> desarrolladoras = new ArrayList<Desarrolladora>();

}