package com.formacion.blockbuster.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "PEDIDOS")
@Data
public class Pedido {
	
	@Id
	@Column(name = "ID_PEDIDO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idPedido;
	
	@Column(name = "COD_REFERENCIA")
	private Integer codReferencia;
	
	@Column(name = "ESTADO")
	private String estado;
	
	@ManyToOne
	private Cliente cliente;
	
	@ManyToOne
	private Juego juego;
	
	@ManyToOne
	private Tienda tienda;

}