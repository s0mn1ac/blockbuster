package com.formacion.blockbuster.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ROLES")
@Data
public class Rol {
	
	@Id
	@Column(name = "ID_ROL")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idRol;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@ManyToMany(cascade = CascadeType.ALL)
	private List<Cliente> clientes = new ArrayList<Cliente>();

}
