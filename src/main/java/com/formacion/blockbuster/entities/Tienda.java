package com.formacion.blockbuster.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TIENDAS")
@Data
public class Tienda {
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idTienda;
	
	@Column(name = "NOMBRE_TIENDA")
	private String nombre;
	
	@Column(name = "DIRECCION")
	private String direccion;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "tienda")
	private List<Pedido> pedidos = new ArrayList<Pedido>();

}