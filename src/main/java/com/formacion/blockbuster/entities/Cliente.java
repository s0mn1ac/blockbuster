package com.formacion.blockbuster.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Entity
@Table(name = "CLIENTES", uniqueConstraints = {@UniqueConstraint(columnNames = {"DOCUMENTO", "USERNAME"})})
@Data
public class Cliente {
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idCliente;
	
	@Column(name = "DOCUMENTO")
	private String documento;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "FECHA_NAC")
	private LocalDate fechaNac;
	
	@Column(name = "CORREO")
	private String correo;
	
	@Column(name = "USERNAME")
	private String user;
	
	@Column(name = "PASSWORD")
	private String pass;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
	private List<Pedido> pedidos = new ArrayList<Pedido>();
	
	@ManyToMany(mappedBy = "clientes")
	private List<Rol> roles = new ArrayList<Rol>();
	
	private String img;

}