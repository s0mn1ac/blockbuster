package com.formacion.blockbuster.validations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;

@Constraint(validatedBy = ValidarDocumento.class) // Esto indica la futura clase que contendrá la lógica de la validación.
@Target({ElementType.METHOD, ElementType.FIELD}) // Indica dónde se aplicará la validación. En este caso a métodos y campos
@Retention(RetentionPolicy.RUNTIME) // Servirá para chequear cuándo se ejecutará la validación. En este caso, en tiempo de ejecución.
public @interface Documento {
	public String message() default DataErrorMessages.DOCUMENTO_NOT_VALID;
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}