package com.formacion.blockbuster.auth;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import com.formacion.blockbuster.entities.Cliente;
import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;
import com.formacion.blockbuster.services.UserService;

@SuppressWarnings("deprecation")
@Component
public class TokenAdditionalInfo implements TokenEnhancer {
	
	@Autowired
	private UserService userService;
	
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		
		Cliente c = userService.findByUser(authentication.getName()).orElseThrow(() -> {
			throw new UsernameNotFoundException(DataErrorMessages.CLIENTE_NOT_FOUND);
		});;
		
		Map<String, Object> info = new HashMap<>();
		
		info.put("name", c.getNombre());
		info.put("img", c.getImg());
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
		
		return accessToken;
		
	}

}
