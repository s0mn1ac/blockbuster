package com.formacion.blockbuster.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.blockbuster.dtos.PedidoDto;
import com.formacion.blockbuster.services.PedidoService;


@Secured({"ROLE_ADMIN", "ROLE_USER"})
@RestController
@CrossOrigin(origins = { "http://localhost:4200"} )
public class PedidoController {
	
	@Autowired
	private PedidoService pedidoService;
	
	@PostMapping("/pedidos")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> transaccion(@Valid @RequestBody PedidoDto pedido) {
		return new ResponseEntity<PedidoDto>(pedidoService.transaccion(pedido), HttpStatus.OK);
	}
	
	@DeleteMapping("/pedidos/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarPedido(@PathVariable long id) {
		pedidoService.eliminarPedido(id);
	}
	
	@GetMapping("/pedidos")
	public List<PedidoDto> mostrarPedidos() {
		return pedidoService.mostrarPedidos();
	}
	
	@GetMapping("/pedidos/page/{page}")
	public Page<PedidoDto> mostrarPedidosPaginados(@PathVariable Integer page) {
		return pedidoService.mostrarPedidosPaginados(page);
	}
	
	@GetMapping("/pedidos/{id}")
	public ResponseEntity<?> mostrarPedido(@PathVariable long id) {
		return new ResponseEntity<PedidoDto>(pedidoService.mostrarPedido(id), HttpStatus.OK);
	}

}
