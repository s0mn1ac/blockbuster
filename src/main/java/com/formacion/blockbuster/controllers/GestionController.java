package com.formacion.blockbuster.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.formacion.blockbuster.dtos.ClienteDto;
import com.formacion.blockbuster.dtos.DesarrolladoraDto;
import com.formacion.blockbuster.dtos.JuegoDto;
import com.formacion.blockbuster.dtos.RolDto;
import com.formacion.blockbuster.dtos.TiendaDto;
import com.formacion.blockbuster.services.GestionService;

@RestController
@CrossOrigin(origins = { "http://localhost:4200"} )
public class GestionController {
	
	
	@Autowired
	private GestionService gestionService;
	
	
	// ----- DESARROLLADORAS ------------------------------------------------------------------------------------------------------
	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/desarrolladoras")
	public ResponseEntity<?> altaDesarrolladora(@Valid @RequestBody DesarrolladoraDto desarrolladora) {
		return new ResponseEntity<DesarrolladoraDto>(gestionService.altaDesarrolladora(desarrolladora), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@PutMapping("/desarrolladoras/{id}")
	public ResponseEntity<?> modDesarrolladoras(@PathVariable long id, @Valid @RequestBody DesarrolladoraDto desarrolladora) {
		return new ResponseEntity<DesarrolladoraDto>(gestionService.modDesarrolladora(id, desarrolladora), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@DeleteMapping("/desarrolladoras/{id}")
	public void bajaDesarroladora(@PathVariable long id) {
		gestionService.bajaDesarroladora(id);
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/desarrolladoras")
	public List<DesarrolladoraDto> mostrarDesarrolladoras() {
		return gestionService.mostrarDesarrolladoras();
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/desarrolladoras/page/{page}")
	public Page<DesarrolladoraDto> mostrarDesarrolladorasPaginadas(@PathVariable Integer page) {
		return gestionService.mostrarDesarrolladorasPaginadas(page);
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/desarrolladoras/{id}")
	public ResponseEntity<?> mostrarDesarrolladora(@PathVariable long id) {
		return new ResponseEntity<DesarrolladoraDto>(gestionService.mostrarDesarrolladora(id), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/desarrolladoras/upload")
	public ResponseEntity<?> subidaImgDesarrolladora(@RequestParam("file") MultipartFile file, @RequestParam("id") long id) {
		return new ResponseEntity<DesarrolladoraDto>(gestionService.subidaImgDesarrolladora(file, id), HttpStatus.CREATED);
	}

	@GetMapping("/desarrolladoras/uploads/img/{img:.+}")
	public ResponseEntity<Resource> vistaImgDesarrolladora(@PathVariable String img) {
		HttpHeaders header = new HttpHeaders();
		header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + img + "\""); 
		return new ResponseEntity<Resource>(gestionService.vistaImgDesarrolladora(img), header, HttpStatus.OK);
	}
	
	
	
	// ----- JUEGOS ---------------------------------------------------------------------------------------------------------------
	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/juegos")
	public ResponseEntity<?> altaJuego(@Valid @RequestBody JuegoDto juego) {
		return new ResponseEntity<JuegoDto>(gestionService.altaJuego(juego), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@PutMapping("/juegos/{id}")
	public ResponseEntity<?> modJuego(@PathVariable long id, @Valid @RequestBody JuegoDto juego) {
		return new ResponseEntity<JuegoDto>(gestionService.modJuego(id, juego), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@DeleteMapping("/juegos/{id}")
	public void bajaJuego(@PathVariable long id) {
		gestionService.bajaJuego(id);
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/juegos")
	public List<JuegoDto> mostrarJuegos() {
		return gestionService.mostrarJuegos();
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/juegos/page/{page}")
	public Page<JuegoDto> mostrarJuegosPaginados(@PathVariable Integer page) {
		return gestionService.mostrarJuegosPaginados(page);
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/juegos/{id}")
	public ResponseEntity<?> mostrarJuego(@PathVariable long id) {
		return new ResponseEntity<JuegoDto>(gestionService.mostrarJuego(id), HttpStatus.OK);
	}
	
	
	
	// ----- TIENDAS --------------------------------------------------------------------------------------------------------------
	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/tiendas")
	public ResponseEntity<?> altaTienda(@Valid @RequestBody TiendaDto tienda) {
		return new ResponseEntity<TiendaDto>(gestionService.altaTienda(tienda), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@PutMapping("/tiendas/{id}")
	public ResponseEntity<?> modTienda(@PathVariable long id, @Valid @RequestBody TiendaDto tienda) {
		return new ResponseEntity<TiendaDto>(gestionService.modTienda(id, tienda), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@DeleteMapping("/tiendas/{id}")
	public void borrarTienda(@PathVariable long id) {
		gestionService.bajaTienda(id);
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/tiendas")
	public List<TiendaDto> mostrarTiendas() {
		return gestionService.mostrarTiendas();
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/tiendas/page/{page}")
	public Page<TiendaDto> mostrarTiendasPaginadas(@PathVariable Integer page) {
		return gestionService.mostrarTiendasPaginadas(page);
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/tiendas/{id}")
	public ResponseEntity<?> mostrarTienda(@PathVariable long id) {
		return new ResponseEntity<TiendaDto>(gestionService.mostrarTienda(id), HttpStatus.OK);
	}
	
	
	//TODO Revisar los HttpStatus. No sé seguro si todos deben devolver OK.
	// ----- CLIENTES ---------------------------------------------------------------------------------------------------------------
	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/clientes")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> altaCliente(@Valid @RequestBody ClienteDto cliente) {
		return new ResponseEntity<ClienteDto>(gestionService.altaCliente(cliente), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@PutMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> modCliente(@PathVariable long id, @Valid @RequestBody ClienteDto cliente) {
		return new ResponseEntity<ClienteDto>(gestionService.modCliente(id, cliente), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@DeleteMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void bajaCliente(@PathVariable long id) {
		gestionService.bajaCliente(id);
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/clientes")
	public List<ClienteDto> mostrarClientes() {
		return gestionService.mostrarClientes();
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/clientes/page/{page}")
	public Page<ClienteDto> mostrarClientesPaginados(@PathVariable Integer page) {
		return gestionService.mostrarClientesPaginados(page);
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/clientes/{id}")
	public ResponseEntity<?> mostrarCliente(@PathVariable long id) {
		return new ResponseEntity<ClienteDto>(gestionService.mostrarCliente(id), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/clientes/upload")
	public ResponseEntity<?> subidaImg(@RequestParam("file") MultipartFile file, @RequestParam("id") long id) {
		return new ResponseEntity<ClienteDto>(gestionService.subidaImg(file, id), HttpStatus.CREATED);
	}

	@GetMapping("/uploads/img/{img:.+}")
	public ResponseEntity<Resource> vistaImg(@PathVariable String img) {
		HttpHeaders header = new HttpHeaders();
		header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + img + "\""); 
		return new ResponseEntity<Resource>(gestionService.vistaImg(img), header, HttpStatus.OK);
	}
	
	
	
	// ----- ROLES ------------------------------------------------------------------------------------------------------------------
	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/roles")
	public ResponseEntity<?> altaRol(@Valid @RequestBody RolDto rol) {
		return new ResponseEntity<RolDto>(gestionService.altaRol(rol), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@PutMapping("/roles/{id}")
	public ResponseEntity<?> modRol(@PathVariable long id, @Valid @RequestBody RolDto rol) {
		return new ResponseEntity<RolDto>(gestionService.modRol(id, rol), HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@DeleteMapping("/roles/{id}")
	public void bajaRol(@PathVariable long id) {
		gestionService.bajaRol(id);
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/roles")
	public List<RolDto> mostrarRoles() {
		return gestionService.mostrarRoles();
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/roles/{id}")
	public ResponseEntity<?> mostrarRol(@PathVariable long id) {
		return new ResponseEntity<RolDto>(gestionService.mostrarRol(id), HttpStatus.OK);
	}
	
}