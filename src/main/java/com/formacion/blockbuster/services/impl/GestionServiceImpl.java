package com.formacion.blockbuster.services.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.formacion.blockbuster.converters.DtoEntityConverter;
import com.formacion.blockbuster.dtos.ClienteDto;
import com.formacion.blockbuster.dtos.DesarrolladoraDto;
import com.formacion.blockbuster.dtos.JuegoDto;
import com.formacion.blockbuster.dtos.RolDto;
import com.formacion.blockbuster.dtos.TiendaDto;
import com.formacion.blockbuster.entities.Cliente;
import com.formacion.blockbuster.entities.Desarrolladora;
import com.formacion.blockbuster.entities.Juego;
import com.formacion.blockbuster.entities.Rol;
import com.formacion.blockbuster.entities.Tienda;
import com.formacion.blockbuster.exceptions.ClienteAlreadyExistsException;
import com.formacion.blockbuster.exceptions.ClienteNotFoundException;
import com.formacion.blockbuster.exceptions.DesarrolladoraAlreadyExistsException;
import com.formacion.blockbuster.exceptions.DesarrolladoraNotFoundException;
import com.formacion.blockbuster.exceptions.JuegoAlreadyExistsException;
import com.formacion.blockbuster.exceptions.JuegoNotFoundException;
import com.formacion.blockbuster.exceptions.RolAlreadyExistsException;
import com.formacion.blockbuster.exceptions.RolNotFoundException;
import com.formacion.blockbuster.exceptions.TiendaAlreadyExistsException;
import com.formacion.blockbuster.exceptions.TiendaNotFoundException;
import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;
import com.formacion.blockbuster.repositories.ClienteRepository;
import com.formacion.blockbuster.repositories.DesarrolladoraRepository;
import com.formacion.blockbuster.repositories.JuegoRepository;
import com.formacion.blockbuster.repositories.RolRepository;
import com.formacion.blockbuster.repositories.TiendaRepository;
import com.formacion.blockbuster.services.GestionService;

@Service
public class GestionServiceImpl implements GestionService {

	private Logger logger = LoggerFactory.getLogger(PedidoServiceImpl.class);
	
	@Autowired
	private BCryptPasswordEncoder enc;

	@Autowired
	private DesarrolladoraRepository desarrolladoraRepository;

	@Autowired
	private JuegoRepository juegoRepository;

	@Autowired
	private TiendaRepository tiendaRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private RolRepository rolRepository;

	@Autowired
	private DtoEntityConverter converter;
	
	
	
	// ----- DESARROLLADORAS ------------------------------------------------------------------------------------------------------

	// altaDesarrolladora: Da de alta una nueva desarrolladora.
	@Override
	@Transactional
	public DesarrolladoraDto altaDesarrolladora(DesarrolladoraDto desarrolladora) {
		
		if (desarrolladoraRepository.findByCif(desarrolladora.getCif()).isPresent()) {
			logger.warn(DataErrorMessages.CIF_ALREADY_BEEN_USED);
			throw new DesarrolladoraAlreadyExistsException(DataErrorMessages.CIF_ALREADY_BEEN_USED);
		}

		desarrolladoraRepository.save(converter.desarrolladoraDtoToEntity(desarrolladora));
		return desarrolladora;
	}

	// altaDesarrolladoras: Da de alta varias desarrolladora.
	@Override
	@Transactional
	public String altaDesarrolladoras(ArrayList<DesarrolladoraDto> desarrolladoras) {
		for (DesarrolladoraDto desarrolladora : desarrolladoras) {
			if (!desarrolladoraRepository.findByCif(desarrolladora.getCif()).isPresent())
				altaDesarrolladora(desarrolladora);
		}
		return DataErrorMessages.ENDED_TRANSACTION;
	}

	// modDesarrolladora: Modifica los datos de una desarrolladora.
	@Override
	@Transactional
	public DesarrolladoraDto modDesarrolladora(long id, DesarrolladoraDto desarrolladora) {
		
		Desarrolladora d = desarrolladoraRepository.findByIdDesarrolladora(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
			throw new DesarrolladoraNotFoundException(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
		});
		
		if (!desarrolladora.getCif().equalsIgnoreCase(d.getCif()) && desarrolladoraRepository.findByCif(desarrolladora.getCif()).isPresent()) {
			logger.warn(DataErrorMessages.CIF_ALREADY_BEEN_USED);
			throw new DesarrolladoraAlreadyExistsException(DataErrorMessages.CIF_ALREADY_BEEN_USED);
		}
		
		d.setCif(desarrolladora.getCif());
		d.setNombre(desarrolladora.getNombre());
		desarrolladoraRepository.save(d);
		
		return desarrolladora;
		
	}

	// bajaDesarrolladora: Da de baja una desarrolladora.
	@Override
	@Transactional
	public void bajaDesarroladora(long id) {
		
		Desarrolladora d = desarrolladoraRepository.findByIdDesarrolladora(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
			throw new DesarrolladoraNotFoundException(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
		});

		for (Juego j : d.getJuegos()) {
			if (j.getDesarrolladoras().size() > 1) {
				j.getDesarrolladoras().remove(d);
				d.getJuegos().remove(j);
			}
		}

		desarrolladoraRepository.delete(d);
		
	}
	
	// mostrarDesarrolladoras: Muestra todas las desarrolladoras registradas.
	@Override
	@Transactional(readOnly = true)
	public List<DesarrolladoraDto> mostrarDesarrolladoras() {
		List<Desarrolladora> dlist = desarrolladoraRepository.findAll();
		List<DesarrolladoraDto> desarrolladoralist = new ArrayList<>();
		for (Desarrolladora d : dlist)
			desarrolladoralist.add(converter.desarrolladoraEntityToDto(d));
		return desarrolladoralist;
	}
	
	// mostrarDesarrolladorasPaginadas: Muestra todas las desarrolladoras registradas, con paginación.
	@Override
	@Transactional(readOnly = true)
	public Page<DesarrolladoraDto> mostrarDesarrolladorasPaginadas(Integer page) {
		
		// TODO Mejorarlo
		if(page < 0) {
			logger.warn(DataErrorMessages.LESS_THAN_ZERO);
			page = 0;
		}
		
		Page<Desarrolladora> paginador = desarrolladoraRepository.findAll(PageRequest.of(page, 5));
		
		Page<DesarrolladoraDto> paginadorDto = paginador.map(new Function<Desarrolladora, DesarrolladoraDto>() {
			@Override
			public DesarrolladoraDto apply(Desarrolladora d) {
				return converter.desarrolladoraEntityToDto(d);
			}
		});

		return paginadorDto;
		
	}
	
	// mostrarDesarrolladora: Muestra los datos de una única desarrolladora.
	@Override
	@Transactional(readOnly = true)
	public DesarrolladoraDto mostrarDesarrolladora(long id) {
		return converter.desarrolladoraEntityToDto(desarrolladoraRepository.findByIdDesarrolladora(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
			throw new DesarrolladoraNotFoundException(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
		}));
	}
	
	// subidaImgDesarrolladora: Permite subir o modificar la imagen del usuario.
	@Override
	@Transactional
	public DesarrolladoraDto subidaImgDesarrolladora(MultipartFile file, long id) {
		
		Desarrolladora d = desarrolladoraRepository.findByIdDesarrolladora(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
			throw new DesarrolladoraNotFoundException(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
		});
		
		
		if(!file.isEmpty()) {
			
			String fileName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename().replace(" ", "");
			
			try {
				Files.copy(file.getInputStream(), Paths.get("resources/imgs/desarrolladoras").resolve(fileName).toAbsolutePath());
			} catch (IOException e) {
				// TODO Revisar la excepción
				e.printStackTrace();
			}
			
			if(d.getImg() != null && d.getImg().length() > 0)
				deleteImgDesarrolladora(d);
			
			d.setImg(fileName);
			desarrolladoraRepository.save(d);
			
		}
		
		return converter.desarrolladoraEntityToDto(d);
		
	}
	
	// vistaImgDesarrolladora: Devuelve la imagen almacenada.
	@Override
	@Transactional
	public Resource vistaImgDesarrolladora(String img) {
		
		Resource res = null;
		
		try {
			res = new UrlResource(Paths.get("resources/imgs/desarrolladoras").resolve(img).toAbsolutePath().toUri());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(!res.exists() && !res.isReadable()) {
			
			try {
				res = new UrlResource(Paths.get("src/main/resources/static/imgs").resolve("empty-file.png").toAbsolutePath().toUri());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//TODO Lanzar Log: Error no se pudo cargar la imagen porque no existe
		}
		
		return res;
		
	}
	
	private void deleteImgDesarrolladora(Desarrolladora d) {
		File prevFile =  Paths.get("resources/imgs/desarrolladoras").resolve(d.getImg()).toAbsolutePath().toFile();
		if(prevFile.exists() && prevFile.canRead())
			prevFile.delete();
	}
	
	
	
	// ----- JUEGOS ---------------------------------------------------------------------------------------------------------------

	// altaJuego: Da de alta un juego nuevo.
	@Override
	@Transactional
	public JuegoDto altaJuego(JuegoDto juego) {
		
		Juego j = converter.juegoDtoToEntity(juego);
		
		if (juegoRepository.findByTitulo(juego.getTitulo()).isPresent()) {
			logger.warn(DataErrorMessages.TITULO_ALREADY_BEEN_USED);
			throw new JuegoAlreadyExistsException(DataErrorMessages.TITULO_ALREADY_BEEN_USED);
		}
		
		List<Desarrolladora> lista = new ArrayList<>();
		
		for (DesarrolladoraDto desarrolladora : juego.getDesarrolladoras()) {
			Desarrolladora d = desarrolladoraRepository.findByIdDesarrolladora(desarrolladora.getId()).orElseThrow(() -> {
				logger.warn(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
				throw new DesarrolladoraNotFoundException(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
			});
			lista.add(d);
			d.getJuegos().add(j);
		}
		
		j.setDesarrolladoras(lista);
		juegoRepository.save(j);
		
		return juego;
		
	}

	// altaJuegos: Da de alta varios juegos.
	@Override
	@Transactional
	public String altaJuegos(ArrayList<JuegoDto> juegos) {
		for (JuegoDto juego : juegos) {
			if (!juegoRepository.findByTitulo(juego.getTitulo()).isPresent())
				altaJuego(juego);
		}
		return DataErrorMessages.ENDED_TRANSACTION;
	}
	
	// modJuego: Modifica los datos de un juego.
	@Override
	@Transactional
	public JuegoDto modJuego(long id, JuegoDto juego) {
		
		Juego j = juegoRepository.findByIdJuego(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.JUEGO_NOT_FOUND);
			throw new JuegoNotFoundException(DataErrorMessages.JUEGO_NOT_FOUND);
		});
		
		if (!juego.getTitulo().equalsIgnoreCase(j.getTitulo()) && juegoRepository.findByTitulo(juego.getTitulo()).isPresent()) {
			logger.warn(DataErrorMessages.TITULO_ALREADY_BEEN_USED);
			throw new JuegoAlreadyExistsException(DataErrorMessages.TITULO_ALREADY_BEEN_USED);
		}
		
		j.setCategoria(juego.getCategoria().toString());
		j.setFechaLanzamiento(juego.getFechaLanzamiento());
		j.setPegi(juego.getPegi());
		j.setTitulo(juego.getTitulo());
		
		for(Desarrolladora d : j.getDesarrolladoras())
			d.getJuegos().remove(j);
		
		List<Desarrolladora> lista = new ArrayList<>();
		
		for(DesarrolladoraDto desarrolladora : juego.getDesarrolladoras()) {
			Desarrolladora d = desarrolladoraRepository.findById(desarrolladora.getId()).orElseThrow(() -> {
				logger.warn(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
				throw new DesarrolladoraNotFoundException(DataErrorMessages.DESARROLLADORA_NOT_FOUND);
			});
			lista.add(d);
			d.getJuegos().add(j);
		}
		
		j.setDesarrolladoras(lista);
		juegoRepository.save(j);
		
		return juego;
		
	}

	// bajaJuego: Da de baja un juego.
	@Override
	@Transactional
	public void bajaJuego(long id) {

		Juego j = juegoRepository.findById(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.JUEGO_NOT_FOUND);
			throw new JuegoNotFoundException(DataErrorMessages.JUEGO_NOT_FOUND);
		});

		for (Desarrolladora d : j.getDesarrolladoras())
			d.getJuegos().remove(j);

		juegoRepository.delete(j);

	}
	
	// mostrarJuegos: Muestra todos los juegos registrados.
	@Override
	@Transactional(readOnly = true)
	public List<JuegoDto> mostrarJuegos() {
		List<Juego> jlist = juegoRepository.findAll();
		List<JuegoDto> juegolist = new ArrayList<>();
		for (Juego j : jlist)
			juegolist.add(converter.juegoEntityToDto(j));
		return juegolist;
	}
	
	// mostrarJuegosPaginados: Muestra todos los juegos registrados, con paginación.
	@Override
	@Transactional(readOnly = true)
	public Page<JuegoDto> mostrarJuegosPaginados(Integer page) {
		
		// TODO Mejorarlo
		if(page < 0) {
			logger.warn(DataErrorMessages.LESS_THAN_ZERO);
			page = 0;
		}
		
		Page<Juego> paginador = juegoRepository.findAll(PageRequest.of(page, 5));
		
		Page<JuegoDto> paginadorDto = paginador.map(new Function<Juego, JuegoDto>() {
			@Override
			public JuegoDto apply(Juego j) {
				return converter.juegoEntityToDto(j);
			}
		});

		return paginadorDto;
		
	}
	
	// mostrarJuego: Muestra los datos de un único juego.
	@Override
	@Transactional(readOnly = true)
	public JuegoDto mostrarJuego(long id) {
		return converter.juegoEntityToDto(juegoRepository.findByIdJuego(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.JUEGO_NOT_FOUND);
			throw new JuegoNotFoundException(DataErrorMessages.JUEGO_NOT_FOUND);
		}));
	}
	
	
	
	// ----- TIENDAS --------------------------------------------------------------------------------------------------------------

	// crearTienda: Registra una nueva tienda.
	@Override
	@Transactional
	public TiendaDto altaTienda(TiendaDto tienda) {
		if (tiendaRepository.findByNombre(tienda.getNombre()).isPresent()) {
			logger.warn(DataErrorMessages.NOMBRE_ALREADY_BEEN_USED);
			throw new TiendaAlreadyExistsException(DataErrorMessages.NOMBRE_ALREADY_BEEN_USED);
		}
		tiendaRepository.save(converter.tiendaDtoToEntity(tienda));
		return tienda;
	}

	// crearTiendas: Registra varias tiendas a la vez.
	@Override
	@Transactional
	public String altaTiendas(ArrayList<TiendaDto> tiendas) {
		for (TiendaDto tienda : tiendas) {
			if (!tiendaRepository.findByNombre(tienda.getNombre()).isPresent())
				altaTienda(tienda);
		}
		return DataErrorMessages.ENDED_TRANSACTION;
	}

	// modTienda: Modifica los datos de una tienda.
	@Override
	@Transactional
	public TiendaDto modTienda(long id, TiendaDto tienda) {
		Tienda t = tiendaRepository.findByIdTienda(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.TIENDA_NOT_FOUND);
			throw new TiendaNotFoundException(DataErrorMessages.TIENDA_NOT_FOUND);
		});
		t.setDireccion(tienda.getDireccion());
		t.setNombre(tienda.getNombre());
		tiendaRepository.save(t);
		return tienda;
	}

	// bajaTienda: Borra una tienda.
	@Override
	@Transactional
	public void bajaTienda(long id) {
		Tienda t = tiendaRepository.findById(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.TIENDA_NOT_FOUND);
			throw new TiendaNotFoundException(DataErrorMessages.TIENDA_NOT_FOUND);
		});
		tiendaRepository.delete(t);
	}
	
	// mostrarTiendas: Muestra todas las tiendas registrados.
	@Override
	@Transactional(readOnly = true)
	public List<TiendaDto> mostrarTiendas() {
		List<Tienda> tlist = tiendaRepository.findAll();
		List<TiendaDto> tiendalist = new ArrayList<>();
		for (Tienda t : tlist)
			tiendalist.add(converter.tiendaEntityToDto(t));
		return tiendalist;
	}
	
	// mostrarTiendasPaginadas: Muestra todas las tiendas registradas, con paginación.
	@Override
	@Transactional(readOnly = true)
	public Page<TiendaDto> mostrarTiendasPaginadas(Integer page) {
		
		// TODO Mejorarlo
		if(page < 0) {
			logger.warn(DataErrorMessages.LESS_THAN_ZERO);
			page = 0;
		}
		
		Page<Tienda> paginador = tiendaRepository.findAll(PageRequest.of(page, 5));
		
		Page<TiendaDto> paginadorDto = paginador.map(new Function<Tienda, TiendaDto>() {
			@Override
			public TiendaDto apply(Tienda t) {
				return converter.tiendaEntityToDto(t);
			}
		});

		return paginadorDto;
		
	}
	
	// mostrarTienda: Muestra los datos de una única tienda.
	@Override
	@Transactional(readOnly = true)
	public TiendaDto mostrarTienda(long id) {
		return converter.tiendaEntityToDto(tiendaRepository.findByIdTienda(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.TIENDA_NOT_FOUND);
			throw new TiendaNotFoundException(DataErrorMessages.TIENDA_NOT_FOUND);
		}));
	}
	
	
	
	// ----- CLIENTES -------------------------------------------------------------------------------------------------------------

	// altaCliente: Crea un cliente.
	@Override
	@Transactional
	public ClienteDto altaCliente(ClienteDto cliente) {
		
		Cliente c = converter.clienteDtoToEntity(cliente);
		
		if (clienteRepository.findByDocumento(cliente.getDocumento()).isPresent()) {
			logger.warn(DataErrorMessages.DOCUMENTO_ALREADY_BEEN_USED);
			throw new ClienteAlreadyExistsException(DataErrorMessages.DOCUMENTO_ALREADY_BEEN_USED);
		}

		if (clienteRepository.findByUser(cliente.getUser()).isPresent()) {
			logger.warn(DataErrorMessages.USER_ALREADY_BEEN_USED);
			throw new ClienteAlreadyExistsException(DataErrorMessages.USER_ALREADY_BEEN_USED);
		}

		List<Rol> lista = new ArrayList<>();

		for (RolDto rol : cliente.getRoles()) {
			Rol r = rolRepository.findByNombre(rol.getNombre().toString()).orElseThrow(() -> {
				logger.warn(DataErrorMessages.ROL_NOT_FOUND);
				throw new RolNotFoundException(DataErrorMessages.ROL_NOT_FOUND);
			});
			lista.add(r);
			r.getClientes().add(c);
		}

		c.setRoles(lista);
		clienteRepository.save(c);
		
		return cliente;
	}

	// altaClientes: Crea varios clientes a la vez.
	@Override
	@Transactional
	public String altaClientes(ArrayList<ClienteDto> clientes) {
		for (ClienteDto cliente : clientes) {
			if (!clienteRepository.findByDocumento(cliente.getDocumento()).isPresent()) {
				if (clienteRepository.findByUser(cliente.getUser()) == null)
					altaCliente(cliente);
			}
		}
		return DataErrorMessages.ENDED_TRANSACTION;
	}

	// modCliente: Modifica los datos de un cliente.
	@Override
	@Transactional
	public ClienteDto modCliente(long id, ClienteDto cliente) {
		
		Cliente c = clienteRepository.findByIdCliente(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.CLIENTE_NOT_FOUND);
			throw new ClienteNotFoundException(DataErrorMessages.CLIENTE_NOT_FOUND);
		});
		
		if (!cliente.getDocumento().equalsIgnoreCase(c.getDocumento()) && clienteRepository.findByDocumento(cliente.getDocumento()).isPresent()) {
			logger.warn(DataErrorMessages.DOCUMENTO_ALREADY_BEEN_USED);
			throw new ClienteAlreadyExistsException(DataErrorMessages.DOCUMENTO_ALREADY_BEEN_USED);
		}
		
		c.setNombre(cliente.getNombre());
		c.setCorreo(cliente.getCorreo());
		c.setFechaNac(cliente.getFechaNac());
		c.setDocumento(cliente.getDocumento());
		c.setUser(cliente.getUser());
		c.setPass(enc.encode(cliente.getPass()));
		
		for(Rol r : c.getRoles())
			r.getClientes().remove(c);
		
		List<Rol> lista = new ArrayList<>();
		
		for (RolDto rol : cliente.getRoles()) {
			Rol r = rolRepository.findByNombre(rol.getNombre().toString()).orElseThrow(() -> {
				logger.warn(DataErrorMessages.ROL_NOT_FOUND);
				throw new RolNotFoundException(DataErrorMessages.ROL_NOT_FOUND);
			});
			lista.add(r);
			r.getClientes().add(c);
		}

		c.setRoles(lista);
		
		clienteRepository.save(c);
		return cliente;
		
	}

	// bajaCliente: Elimina un cliente.
	@Override
	@Transactional
	public void bajaCliente(long id) {
		Cliente c = clienteRepository.findByIdCliente(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.CLIENTE_NOT_FOUND);
			throw new ClienteNotFoundException(DataErrorMessages.CLIENTE_NOT_FOUND);
		});
		for (Rol r : c.getRoles())
			r.getClientes().remove(c);
		if(c.getImg() != null && c.getImg().length() > 0)
			deleteImg(c);
		clienteRepository.delete(c);
	}
	
	// mostrarClientes: Muestra todos los clientes registrados.
	@Override
	@Transactional(readOnly = true)
	public List<ClienteDto> mostrarClientes() {
		List<Cliente> clist = clienteRepository.findAll();
		List<ClienteDto> clientelist = new ArrayList<>();
		for (Cliente c : clist)
			clientelist.add(converter.clienteEntityToDto(c));
		return clientelist;
	}
	
	// mostrarClientesPaginados: Muestra todos los clientes registrados, con paginación.
	@Override
	@Transactional(readOnly = true)
	public Page<ClienteDto> mostrarClientesPaginados(Integer page) {
		
		// TODO Mejorarlo
		if(page < 0) {
			logger.warn(DataErrorMessages.LESS_THAN_ZERO);
			page = 0;
		}
		
		Page<Cliente> paginador = clienteRepository.findAll(PageRequest.of(page, 5));
		
		Page<ClienteDto> paginadorDto = paginador.map(new Function<Cliente, ClienteDto>() {
			@Override
			public ClienteDto apply(Cliente c) {
				return converter.clienteEntityToDto(c);
			}
		});

		return paginadorDto;
		
	}
	
	// mostrarCliente: Muestra los datos de un único cliente.
	@Override
	@Transactional(readOnly = true)
	public ClienteDto mostrarCliente(long id) {
		return converter.clienteEntityToDto(clienteRepository.findByIdCliente(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.CLIENTE_NOT_FOUND);
			throw new ClienteNotFoundException(DataErrorMessages.CLIENTE_NOT_FOUND);
		}));
	}
	
	// subidaImg: Permite subir o modificar la imagen del usuario.
	@Override
	@Transactional
	public ClienteDto subidaImg(MultipartFile file, long id) {
		
		Cliente c = clienteRepository.findByIdCliente(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.CLIENTE_NOT_FOUND);
			throw new ClienteNotFoundException(DataErrorMessages.CLIENTE_NOT_FOUND);
		});
		
		
		if(!file.isEmpty()) {
			
			String fileName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename().replace(" ", "");
			
			try {
				Files.copy(file.getInputStream(), Paths.get("resources/imgs").resolve(fileName).toAbsolutePath());
			} catch (IOException e) {
				// TODO Revisar la excepción
				e.printStackTrace();
			}
			
			if(c.getImg() != null && c.getImg().length() > 0)
				deleteImg(c);
			
			c.setImg(fileName);
			clienteRepository.save(c);
			
		}
		
		return converter.clienteEntityToDto(c);
		
	}
	
	// vistaImg: Devuelve la imagen almacenada.
	@Override
	@Transactional
	public Resource vistaImg(String img) {
		
		Resource res = null;
		
		try {
			res = new UrlResource(Paths.get("resources/imgs").resolve(img).toAbsolutePath().toUri());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(!res.exists() && !res.isReadable()) {
			
			try {
				res = new UrlResource(Paths.get("src/main/resources/static/imgs").resolve("empty-user.png").toAbsolutePath().toUri());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//TODO Lanzar Log: Error no se pudo cargar la imagen porque no existe
		}
		
		return res;
		
	}
	
	private void deleteImg(Cliente c) {
		File prevFile =  Paths.get("resources/imgs").resolve(c.getImg()).toAbsolutePath().toFile();
		if(prevFile.exists() && prevFile.canRead())
			prevFile.delete();
	}
	
	
	
	
	// ----- ROLES ----------------------------------------------------------------------------------------------------------------

	// altaRol: Da de alta un nuevo rol.
	@Override
	public RolDto altaRol(RolDto rol) {
		
		if (rolRepository.findByNombre(rol.getNombre().toString()).isPresent()) {
			logger.warn(DataErrorMessages.NOMBRE_ALREADY_BEEN_USED);
			throw new RolAlreadyExistsException(DataErrorMessages.NOMBRE_ALREADY_BEEN_USED);
		}

		Rol r = new Rol();
		
		r.setNombre(rol.getNombre().toString());
		rolRepository.save(r);
		
		return rol;
		
	}

	// modRol: Modifica el nombre de un rol.
	@Override
	public RolDto modRol(long id, RolDto rol) {
		
		Rol r = rolRepository.findByIdRol(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.ROL_NOT_FOUND);
			throw new RolNotFoundException(DataErrorMessages.ROL_NOT_FOUND);
		});
		
		if (!rol.getNombre().toString().equalsIgnoreCase(r.getNombre()) & rolRepository.findByNombre(rol.getNombre().toString()) != null) {
			logger.warn(DataErrorMessages.NOMBRE_ALREADY_BEEN_USED);
			throw new RolAlreadyExistsException(DataErrorMessages.NOMBRE_ALREADY_BEEN_USED);
		}

		r.setNombre(rol.getNombre().toString());
		rolRepository.save(r);
		
		return rol;
		
	}

	// bajaRol: Elimina un rol.
	@Override
	public void bajaRol(long id) {
		
		Rol r = rolRepository.findById(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.ROL_NOT_FOUND);
			throw new RolNotFoundException(DataErrorMessages.ROL_NOT_FOUND);
		});

		for (Cliente c : r.getClientes()) {
			if (c.getRoles().size() > 1) {
				c.getRoles().remove(r);
				r.getClientes().remove(c);
			}
		}

		rolRepository.delete(r);
		
	}
	
	// mostrarRoles: Muestra todos los roles registrados.
	@Override
	@Transactional(readOnly = true)
	public List<RolDto> mostrarRoles() {
		
		List<Rol> rlist = rolRepository.findAll();
		List<RolDto> rollist = new ArrayList<>();
		
		for (Rol r : rlist)
			rollist.add(converter.rolEntityToDto(r));
		
		return rollist;
		
	}
	
	// mostrarRol: Muestra los datos de un único rol.
	@Override
	@Transactional(readOnly = true)
	public RolDto mostrarRol(long id) {
		
		return converter.rolEntityToDto(rolRepository.findByIdRol(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.ROL_NOT_FOUND);
			throw new RolNotFoundException(DataErrorMessages.ROL_NOT_FOUND);
		}));
		
	}

}