package com.formacion.blockbuster.services.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.blockbuster.entities.Cliente;
import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;
import com.formacion.blockbuster.repositories.ClienteRepository;
import com.formacion.blockbuster.services.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService, UserDetailsService {
	
	private Logger logger = LoggerFactory.getLogger(PedidoServiceImpl.class);
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Cliente c = clienteRepository.findByUser(username).orElseThrow(() -> {
			logger.warn(DataErrorMessages.CLIENTE_NOT_FOUND);
			throw new UsernameNotFoundException(DataErrorMessages.CLIENTE_NOT_FOUND);
		});;
		
		List<GrantedAuthority> roles = c.getRoles().stream().map(rol -> new SimpleGrantedAuthority(rol.getNombre())).collect(Collectors.toList());
		
		return new User(c.getUser(), c.getPass(), true, true, true, true, roles);
		
	}

	@Override
	public Optional<Cliente> findByUser(String user) {
		return clienteRepository.findByUser(user);
	}
	
//	private Collection<? extends GrantedAuthority> getAuthorities(Collection<Rol> roles) {
//		return getGrantedAuthorities(getPrivileges(roles));
//	}
//	
//	private List<String> getPrivileges(Collection<Rol> roles) {
//		  
//        List<String> privileges = new ArrayList<>();
//        for(Rol rol : roles)
//	        privileges.add(rol.getNombre());
//		        
//        return privileges;
//    }
// 
//    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
//        List<GrantedAuthority> authorities = new ArrayList<>();
//        for (String privilege : privileges) {
//            authorities.add(new SimpleGrantedAuthority(privilege));
//        }
//        return authorities;
//    }

}
