package com.formacion.blockbuster.services.impl;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.blockbuster.converters.DtoEntityConverter;
import com.formacion.blockbuster.dtos.PedidoDto;
import com.formacion.blockbuster.entities.Cliente;
import com.formacion.blockbuster.entities.Juego;
import com.formacion.blockbuster.entities.Pedido;
import com.formacion.blockbuster.entities.Tienda;
import com.formacion.blockbuster.exceptions.AlreadyBeenRentedException;
import com.formacion.blockbuster.exceptions.ClienteNotFoundException;
import com.formacion.blockbuster.exceptions.EdadNotValidException;
import com.formacion.blockbuster.exceptions.JuegoNotFoundException;
import com.formacion.blockbuster.exceptions.JuegoNotYetLaunchedException;
import com.formacion.blockbuster.exceptions.PedidoNotFoundException;
import com.formacion.blockbuster.exceptions.TiendaNotFoundException;
import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;
import com.formacion.blockbuster.repositories.ClienteRepository;
import com.formacion.blockbuster.repositories.JuegoRepository;
import com.formacion.blockbuster.repositories.PedidoRepository;
import com.formacion.blockbuster.repositories.TiendaRepository;
import com.formacion.blockbuster.services.PedidoService;

@Service
public class PedidoServiceImpl implements PedidoService {
	
	private Logger logger = LoggerFactory.getLogger(PedidoServiceImpl.class);
		
	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private JuegoRepository juegoRepository;
	
	@Autowired
	private TiendaRepository tiendaRepository;
	
	@Autowired
	private DtoEntityConverter converter;

	// transaccion: Permite al cliente alquilar o comprar un juego.
	@Override
	@Transactional
	public PedidoDto transaccion(PedidoDto pedido) {	
			
		Cliente c = clienteRepository.findByIdCliente(pedido.getCliente().getId()).orElseThrow(() -> {logger.warn(DataErrorMessages.CLIENTE_NOT_FOUND); throw new ClienteNotFoundException(DataErrorMessages.CLIENTE_NOT_FOUND);});
		Juego j = juegoRepository.findByIdJuego(pedido.getJuego().getId()).orElseThrow(() -> {logger.warn(DataErrorMessages.JUEGO_NOT_FOUND); throw new JuegoNotFoundException(DataErrorMessages.JUEGO_NOT_FOUND);});
		Tienda t = tiendaRepository.findByIdTienda(pedido.getTienda().getId()).orElseThrow(() -> {logger.warn(DataErrorMessages.TIENDA_NOT_FOUND); throw new TiendaNotFoundException(DataErrorMessages.TIENDA_NOT_FOUND);});
		
		Pedido p = new Pedido();
		
		switch (pedido.getEstado().toString()) {
		case "ALQUILADO":
			
			if(!comprobarFechaLanzamiento(j.getFechaLanzamiento())) {
				logger.warn(DataErrorMessages.JUEGO_NOT_READY);
				throw new JuegoNotYetLaunchedException(DataErrorMessages.JUEGO_NOT_READY);
			}
				
			if(pedidoRepository.findClienteJuego(c, pedido.getEstado().toString()) != 0) {
				logger.warn(DataErrorMessages.ALREADY_BEEN_RENTED);
				throw new AlreadyBeenRentedException(DataErrorMessages.ALREADY_BEEN_RENTED);
			}
			
			if(!calcularEdad(c.getFechaNac(), j.getPegi())) {
				logger.warn(DataErrorMessages.NOT_OLD_ENOUGH);
				throw new EdadNotValidException(DataErrorMessages.NOT_OLD_ENOUGH);
			}
			
			p.setCodReferencia(generadorReferencia());
			p.setEstado(pedido.getEstado().toString());
			p.setCliente(c);
			p.setJuego(j);
			p.setTienda(t);
			
			c.getPedidos().add(p);
			j.getPedidos().add(p);
			t.getPedidos().add(p);
			
			pedidoRepository.save(p);
			break;
			
		case "COMPRADO":
			
			if(!comprobarFechaLanzamiento(j.getFechaLanzamiento())) {
				logger.warn(DataErrorMessages.JUEGO_NOT_READY);
				throw new JuegoNotYetLaunchedException(DataErrorMessages.JUEGO_NOT_READY);
			}
			
			if(!calcularEdad(c.getFechaNac(), j.getPegi())) {
				logger.warn(DataErrorMessages.NOT_OLD_ENOUGH);
				throw new EdadNotValidException(DataErrorMessages.NOT_OLD_ENOUGH);
			}

			p.setCodReferencia(generadorReferencia());
			p.setEstado(pedido.getEstado().toString());
			p.setCliente(c);
			p.setJuego(j);
			p.setTienda(t);
			
			c.getPedidos().add(p);
			j.getPedidos().add(p);
			t.getPedidos().add(p);
			
			pedidoRepository.save(p);
			break;

		}
		
		return pedido;
			
	}
	
	// TODO FINAL
	
	// devolverJuego: Devuelve un juego.
	@Override
	@Transactional
	public void devolverJuego(long id) {
		
		Pedido p = pedidoRepository.findByIdPedido(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.PEDIDO_NOT_FOUND);
			throw new PedidoNotFoundException(DataErrorMessages.PEDIDO_NOT_FOUND);
		});

		pedidoRepository.delete(p);
		
	}
	
	// eliminarPedido: Elimina un pedido.
	@Override
	@Transactional
	public void eliminarPedido(long id) {
		
		Pedido p = pedidoRepository.findByIdPedido(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.PEDIDO_NOT_FOUND);
			throw new PedidoNotFoundException(DataErrorMessages.PEDIDO_NOT_FOUND);
		});

		pedidoRepository.delete(p);
		
	}
	
	//mostrarPedidos: Muestra todos los pedidos registrados.
	@Override
	@Transactional(readOnly = true)
	public List<PedidoDto> mostrarPedidos() {
		List<Pedido> plist = pedidoRepository.findAll();
		List<PedidoDto> pedidolist = new ArrayList<>();
		for (Pedido p : plist)
			pedidolist.add(converter.pedidoEntityToDto(p));
		return pedidolist;
	}
	
	// mostrarPedidosPaginados: Muestra todos los pedidos registrados, con paginación.
	@Override
	@Transactional(readOnly = true)
	public Page<PedidoDto> mostrarPedidosPaginados(Integer page) {
		
		// TODO Mejorarlo
		if(page < 0) {
			logger.warn(DataErrorMessages.LESS_THAN_ZERO);
			page = 0;
		}
		
		Page<Pedido> paginador = pedidoRepository.findAll(PageRequest.of(page, 5));
		
		Page<PedidoDto> paginadorDto = paginador.map(new Function<Pedido, PedidoDto>() {
			@Override
			public PedidoDto apply(Pedido p) {
				return converter.pedidoEntityToDto(p);
			}
		});

		return paginadorDto;
		
	}
	
	// mostrarPedido: Muestra los datos de un único pedido.
	@Override
	@Transactional(readOnly = true)
	public PedidoDto mostrarPedido(long id) {
		return converter.pedidoEntityToDto(pedidoRepository.findByIdPedido(id).orElseThrow(() -> {
			logger.warn(DataErrorMessages.PEDIDO_NOT_FOUND);
			throw new PedidoNotFoundException(DataErrorMessages.PEDIDO_NOT_FOUND);
		}));
	}
	
	
	
	// ----- FUNCIONES PRIVADAS ---------------------------------------------------------------------------------------------------
	
	// calcularEdad: Función privada. Comprueba si la edad del cliente es acorde al PEGI del juego que quiera alquilar/comprar.
	private boolean calcularEdad(LocalDate fechaNac, Integer pegi) {
		return (Period.between(fechaNac, LocalDate.now()).getYears() > pegi);
	}
	
	// generadorReferencia: Función privada. Genera un código de referencia automáticamente.
	private Integer generadorReferencia() {
		Integer ref = pedidoRepository.findMaxRef();
		if(ref == null)
			ref = 10000100;
		return ref;
	}
	
	// comprobarFechaLanzamiento: Función privada. Comprueba si un juego aún no ha sido lanzado.
	private boolean comprobarFechaLanzamiento(LocalDate fechaLanzamiento) {
		return fechaLanzamiento.isBefore(LocalDate.now());
	}

}
