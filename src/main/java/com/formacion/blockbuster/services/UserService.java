package com.formacion.blockbuster.services;

import java.util.Optional;

import com.formacion.blockbuster.entities.Cliente;

public interface UserService {
	
	public Optional<Cliente> findByUser(String user);

}
