package com.formacion.blockbuster.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.formacion.blockbuster.dtos.PedidoDto;

@Service
public interface PedidoService {	

	public PedidoDto transaccion(PedidoDto pedido);
	public PedidoDto mostrarPedido(long id);
	public List<PedidoDto> mostrarPedidos();
	public Page<PedidoDto> mostrarPedidosPaginados(Integer page);
	public void devolverJuego(long id);
	public void eliminarPedido(long id);
	
}
