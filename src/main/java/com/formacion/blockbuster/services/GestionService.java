package com.formacion.blockbuster.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.formacion.blockbuster.dtos.ClienteDto;
import com.formacion.blockbuster.dtos.DesarrolladoraDto;
import com.formacion.blockbuster.dtos.JuegoDto;
import com.formacion.blockbuster.dtos.RolDto;
import com.formacion.blockbuster.dtos.TiendaDto;

@Service
public interface GestionService {
		
	// ----- DESARROLLADORAS ------------------------------------------------------------------------------------------------------
	
	public DesarrolladoraDto altaDesarrolladora(DesarrolladoraDto desarrolladora);
	public String altaDesarrolladoras(ArrayList<DesarrolladoraDto> desarrolladoras);
	public DesarrolladoraDto modDesarrolladora(long id, DesarrolladoraDto desarrolladora);
	public void bajaDesarroladora(long id);
	public List<DesarrolladoraDto> mostrarDesarrolladoras();
	public Page<DesarrolladoraDto> mostrarDesarrolladorasPaginadas(Integer page);
	public DesarrolladoraDto mostrarDesarrolladora(long id);	
	public DesarrolladoraDto subidaImgDesarrolladora(MultipartFile file, long id);
	public Resource vistaImgDesarrolladora(String img);
	
	
	
	// ----- JUEGOS ---------------------------------------------------------------------------------------------------------------
	
	public JuegoDto altaJuego(JuegoDto juego);
	public String altaJuegos(ArrayList<JuegoDto> juegos);
	public JuegoDto modJuego(long id, JuegoDto juego);
	public void bajaJuego(long id);
	public List<JuegoDto> mostrarJuegos();
	public Page<JuegoDto> mostrarJuegosPaginados(Integer page);
	public JuegoDto mostrarJuego(long id);
	
	
	
	// ----- TIENDAS --------------------------------------------------------------------------------------------------------------
	
	public TiendaDto altaTienda(TiendaDto tienda);
	public String altaTiendas(ArrayList<TiendaDto> tiendas);
	public TiendaDto modTienda(long id, TiendaDto tienda);
	public void bajaTienda(long id);
	public List<TiendaDto> mostrarTiendas();
	public Page<TiendaDto> mostrarTiendasPaginadas(Integer page);
	public TiendaDto mostrarTienda(long id);

	
	
	// ----- CLIENTES -------------------------------------------------------------------------------------------------------------
	
	public ClienteDto altaCliente(ClienteDto cliente);
	public String altaClientes(ArrayList<ClienteDto> clientes);
	public ClienteDto modCliente(long id, ClienteDto cliente);
	public void bajaCliente(long id);
	public List<ClienteDto> mostrarClientes();
	public Page<ClienteDto> mostrarClientesPaginados(Integer page);
	public ClienteDto mostrarCliente(long id);
	public ClienteDto subidaImg(MultipartFile file, long id);
	public Resource vistaImg(String img);
	
	
	
	// ----- ROLES ----------------------------------------------------------------------------------------------------------------

	public RolDto altaRol(RolDto rol);
	public RolDto modRol(long id, RolDto rol);
	public void bajaRol(long id);
	public List<RolDto> mostrarRoles();
	public RolDto mostrarRol(long id);
	
}