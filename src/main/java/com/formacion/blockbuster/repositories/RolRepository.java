package com.formacion.blockbuster.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.formacion.blockbuster.entities.Rol;

public interface RolRepository extends JpaRepository<Rol, Long> {

	public Optional<Rol> findByNombre(String nombre);
	public Optional<Rol> findByIdRol(Long idRol);

}
