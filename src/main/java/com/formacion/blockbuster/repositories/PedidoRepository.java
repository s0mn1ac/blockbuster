package com.formacion.blockbuster.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.formacion.blockbuster.entities.Cliente;
import com.formacion.blockbuster.entities.Pedido;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long> {
	
	@Query("SELECT MAX(p.codReferencia) + 1 FROM Pedido p")
	public Integer findMaxRef();
	
	@Query("SELECT COUNT(p.cliente) FROM Pedido p WHERE p.cliente = ?1 AND UPPER(p.estado) = ?2")
	public Integer findClienteJuego(Cliente c, String e);
	
	public List<Pedido> findByCliente(Cliente cliente);
	public Optional<Pedido> findByCodReferencia(Integer codReferencia);
	public Optional<Pedido> findByIdPedido(Long idPedido);
	
	public Page<Pedido> findAll(Pageable pageable);
	
}
