package com.formacion.blockbuster.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.blockbuster.entities.Desarrolladora;

@Repository
public interface DesarrolladoraRepository extends JpaRepository<Desarrolladora, Long> {
	
	public Optional<Desarrolladora> findByIdDesarrolladora(Long idDesarrolladora);
	public Optional<Desarrolladora> findByCif(String cif);
	
	public Page<Desarrolladora> findAll(Pageable pageable);
}