package com.formacion.blockbuster.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.blockbuster.entities.Tienda;

@Repository
public interface TiendaRepository extends JpaRepository<Tienda, Long> {
	
	public Optional<Tienda> findByIdTienda(Long idTienda);
	public Optional<Tienda> findByNombre(String tienda);
	
	public Page<Tienda> findAll(Pageable pageable);

}
