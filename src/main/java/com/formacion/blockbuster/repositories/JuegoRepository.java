package com.formacion.blockbuster.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.blockbuster.entities.Juego;

@Repository
public interface JuegoRepository extends JpaRepository<Juego, Long> {
	
	public Optional<Juego> findByIdJuego(Long idJuego);
	public Optional<Juego> findByTitulo(String titulo);
	
	public Page<Juego> findAll(Pageable pageable);

}
