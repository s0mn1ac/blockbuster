package com.formacion.blockbuster.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.blockbuster.entities.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
	public Optional<Cliente> findByIdCliente(Long idCliente);
	public Optional<Cliente> findByDocumento(String documento);
	public Optional<Cliente> findByUser(String user);
	
	public Page<Cliente> findAll(Pageable pageable);

}
