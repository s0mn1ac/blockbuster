package com.formacion.blockbuster;

//import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

//import com.formacion.blockbuster.enums.EnumRol;
//import com.formacion.blockbuster.exceptions.messages.DataErrorMessages;
import com.formacion.blockbuster.services.impl.UserServiceImpl;

@EnableGlobalMethodSecurity(securedEnabled = true)
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserServiceImpl userService;
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	} 
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
	}
	
	@Bean("authenticationManager")
	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	
	
	
	// TODO Verificar que esta sea la manera correcta de restringir el acceso.
	// La siguiente función indica que cualquier petición que entre deberá estar autenticada.
	// Añadiendo .csrf().disable() permitimos conexiones externas.
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		http.authorizeRequests()
//			.antMatchers("/cliente/*").hasAuthority(EnumRol.ADMIN.toString())
//			.antMatchers("/tienda/*").hasAuthority(EnumRol.ADMIN.toString())
//			.antMatchers("/juego/*").hasAuthority(EnumRol.ADMIN.toString())
//			.antMatchers("/desarrolladora/*").hasAuthority(EnumRol.ADMIN.toString())
//			.antMatchers("/rol/*").hasAuthority(EnumRol.ADMIN.toString())
			//.antMatchers("/pedido/*").hasAnyAuthority(EnumRol.ADMIN.toString(), EnumRol.USER.toString())
//			.and()
//			.exceptionHandling().accessDeniedHandler((request, response, ex) -> {response.sendError(HttpServletResponse.SC_FORBIDDEN, DataErrorMessages.FORBIDDEN);}).and()
//			.csrf().disable().httpBasic();
//		http.cors();
//	}
	
}