package com.formacion.blockbuster.enums;

public enum EnumCategoria {
	ACCION, ARCADE, DEPORTIVO, ESTRATEGIA, SIMULACION, MUSICAL, SHOOTER, AVENTURA, ROL, CARRERAS
}
