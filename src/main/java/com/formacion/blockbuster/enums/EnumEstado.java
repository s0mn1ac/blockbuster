package com.formacion.blockbuster.enums;

public enum EnumEstado {
	ALQUILADO, COMPRADO, DEVUELTO
}
